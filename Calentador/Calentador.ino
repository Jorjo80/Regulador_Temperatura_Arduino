#include <LiquidCrystal.h>
int mosfet=3;
int pote = A1;
int temp = A2;
int rojo = 13;
int verde = 12;
int azul = 11;
int duty;
int ciento;
float valor;
float temperatura;
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
int luzlcd=10;
void setup()
{
  pinMode(mosfet,OUTPUT);
  pinMode(pote,INPUT);
  pinMode(temp,INPUT);
  pinMode(rojo,OUTPUT);
  pinMode(verde,OUTPUT);
  pinMode(azul,OUTPUT);
  Serial.begin(9600);  //para indicar la velocidad de comunicaciÃ³n con el ordenador.
}
void loop()
{
  lcd.begin(16, 2);
  lecturapote();
  delay(10);
  lecturatemp();
  calentar();
  disp();
  luces();
  delay(1000);
  //Serial.print(temperatura);
  Serial.print("\n");
  Serial.print(duty);
  
}

void disp()
{
  //Serial.print(duty);
  lcd.setCursor(0,0);
  lcd.print("Set ");
  Serial.print("Set: ");
  lcd.print(valor);
  lcd.print("oC");
  Serial.print("oC       ");
  lcd.setCursor(13,0);
  lcd.print(ciento);
  Serial.print(ciento);
  lcd.setCursor(0,1);
  lcd.print("Get: ");
  Serial.print("       Get: ");
  lcd.print(temperatura);
  lcd.print("oC");
  Serial.print("oC       ");
  Serial.print("\n");
}

void lecturapote()
{
  valor=(analogRead(pote));
  valor = valor/(10.23); 
  
}
float lecturatemp()
{
  int n=0;
  float temp3=0;
  do
  {
    temperatura = analogRead(temp);
    temperatura = 5.0*temperatura*100.0/1024.0;
    if(n>0)
    {
      temp3=temp3+temperatura;
    }
    n++;
  }while(n<10);
  temperatura = temp3/9;
}

void luces()
{
  if (temperatura < 10 )
  {
    digitalWrite(azul,HIGH);
    digitalWrite(verde,LOW);
    digitalWrite(rojo,LOW);
  }
  if (temperatura > 10 && temperatura < 30)
  {
    digitalWrite(azul,HIGH);
    digitalWrite(verde,HIGH);
    digitalWrite(rojo,LOW);
  }
  if (temperatura > 30 && temperatura < 40)
  {
    digitalWrite(azul,LOW);
    digitalWrite(verde,HIGH);
    digitalWrite(rojo,LOW);
  }
  if (temperatura > 40 && temperatura < 50)
  {
    digitalWrite(azul,LOW);
    digitalWrite(verde,HIGH);
    digitalWrite(rojo,HIGH);
  }
  if (temperatura > 50)
  {
    digitalWrite(azul,LOW);
    digitalWrite(verde,LOW);
    digitalWrite(rojo,HIGH);
  }
}
void calentar()
{
   duty = (valor - temperatura)*100;
   if(duty > 255)
   {
    duty=255;
   }
   if(duty < 0)
   {
    duty = 0;
   }
   analogWrite(mosfet,duty);
   ciento = (duty*100/255);
   //Serial.print(ciento);
}

